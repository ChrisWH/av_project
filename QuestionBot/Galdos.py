class Bot(object):

    def __init__(self, name):
        self.name = name
        self.conversation = []

    def answer(self, input):
        pass

    def random_answer(self, opetional_answers):
        answers = opetional_answers[:]
        for i in range(min(5, len(answers))):
            r = random.randint(0, len(answers) - 1)
            resp = answers[r]
            if not self.AnswerWasAreadySaid(resp):
                self.conversation.append(resp)
                return resp
            list(answers).remove(resp)
        return "Let's talk about something different"

    def AnswerWasAreadySaid(self, response):
        if not response in self.conversation:
            self.conversation.append(response)
            return False
        else:
            return True


import random
class BasicBot(Bot):
    def __init__(self, name):
        super(BasicBot, self).__init__(name)
        self.topics = {'name':["My name is %s"%self.name, "You can call me %s"%self.name, "I am %s"%self.name]
            , "weather":["It's too hot", 'way too cold']
            , 'profession':['I am a System Admin', "I am a Programmer", "I am a Wizard"]}

    def greet(self):
        greetings = ["What's up?", "Hello there!", "How are you?", "How do you do?", "Howdy!",
                    "Do you want to make America great again?", "Hi!"]
        return self.random_answer(greetings)

    def stupid_answer(self):
        answers = ["Thanks!", "42", "The cake is a lie", "I didn't understand.", "let's change the topic",
                   "Nice Weather we are having", "Could you tell me where the next train station is?"]
        return self.random_answer(answers)

    def answer(self, input):
        self.conversation.append(input)
        if not input:
            return self.greet()
            intro = self.random_answer(introduction)
            self.conversation.append(intro)
            return intro.format(self.name)
        else:
            if input.endswith('?') or random.randint(0,1) == 1:
                for word in self.topics.keys():
                    if input.__contains__(word):
                        return self.random_answer(self.topics[word])
            return self.stupid_answer()


class User(Bot):
    def __init__(self, name):
        super(User, self).__init__(name)

    def answer(self, input_text):
        if input_text:
            print(self.name + " Please answer the following question: ")
        else:
            print(self.name + " Please start a conversation: ")
        return input(' ->' + input_text + " ")


def pereform_conversation(bot1, bot2):
    input_text = ''
    while True:
        input_text = bot1.answer(input_text)
        if not input_text:
            break
        print(bot1.name + ': ' + input_text)

        input_text = bot2.answer(input_text)
        if not input_text:
            break
        print(bot2.name + ': ' + input_text)
    print("Conversation ended")

def main():
    user1 = BasicBot("Artem")
    user2 = User("Paul")
    pereform_conversation(user1, user2)

main()