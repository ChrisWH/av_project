import urllib.request as req
import urllib.error as urlerror
import json
from SPARQLWrapper import SPARQLWrapper, JSON


class TranslateSQID:
    """
    Handles conversion to an from Source Query IDs (SQID)
    """
    def __init__(self, term="", sqid=""):
        """
        :param term: the term to be translate eg. 'Clint Eastwood'
        :param sqid: the SQID to be looked up e.g., Q43203.
        """
        if term != "":
            self.sqid = self.getSQID(term)
        elif sqid != "":
            self.sqid = sqid
        else:
            raise Exception("Invalid object creation")

    def getSQID(self, term):
        """
        Finds a single SQID or returns a list of SQIDs that were found in connection with the given term
        :param term: the term to be translate eg. 'Clint Eastwood'
        :return: a list of SQIDs.
        """
        urlTerm = term.replace(' ', '%20')

        sUrl = "https://www.wikidata.org/w/api.php?action=wbsearchentities&search={}&language=en&format=json".format(
            urlTerm)
        resp = req.urlopen(sUrl).read()
        jdata = json.loads(resp.decode("UTF-8"))
        try:
            if jdata['success'] == 1:
                search = jdata['search']
                for qry in search:
                    if qry['label'] == term:
                        if 'disambiguation' not in str(qry['description']):
                            return qry['id']
                matches = list()
                for qry in search:
                    if qry['label'].lower() == term.lower():
                        if 'disambiguation' not in str(qry['description']):
                            matches.append(qry['id'])
                if len(matches) > 0:
                    if len(matches) == 1:
                        return matches[0]
                    else:
                        return matches
                else:
                    return list(qry['id'] for qry in search)
        except KeyError:
            raise LookupError("The search did not return a valid term.")

    def reverseLookup(self, sqid=""):
        """
        Returns the name and description of a entered/member sqid
        :param sqid: the ID to search for or '' for the member sqid assuming the object was initialized with one.
        :return: a list of dictionaries containing name and description.
        """
        sparql = SPARQLWrapper('https://query.wikidata.org/sparql')
        sparql.setReturnFormat(JSON)
        if sqid == "":
            if self.sqid == "":
                raise LookupError("No Know SQID was given")
            else:
                sqid = self.sqid
        try:
            lookup = """Select distinct ?label ?description
            Where{{	wd:{sqid} rdfs:label ?label Filter(lang(?label)="en").
            wd:{sqid} skos:altLabel ?alt Filter(lang(?alt)="en").
            wd:{sqid}  schema:description ?description Filter(lang(?description)="en").
            SERVICE wikibase:label {{bd:serviceParam wikibase:language "en"}}}}""".format(sqid=sqid)
            sparql.setQuery(lookup)
            desc = sparql.query().convert()
            desc = desc['results']['bindings'][0]
            res = (desc['label']['value'], desc['description']['value'])
            return res

        except urlerror.HTTPError:
            raise LookupError("The given SQID does not exist")

print("Clint Eastwood's sqid:  " + TranslateSQID(term="Clint Eastwood").sqid)
print("Sqid lookup: " + str(TranslateSQID(sqid="Q666").reverseLookup()))


class WikidataLink:
    """
    Creates a connection to wikidata using already quarried sqids
    """

    def __init__(self, sqid):
        if (isinstance(sqid, list)):
            self.sqid=sqid[0]
        else:
            self.sqid=sqid
        self.description = ""
        spEnd = 'https://query.wikidata.org/sparql'
        self.sparql = SPARQLWrapper(spEnd)
        self.sparql.setReturnFormat(JSON)
        self.age = None
        self.dob = None
        self.dod = None

    def getEntityData(self, sqid=""):
        if sqid == "": sqid = self.sqid
        entRequest = "http://www.wikidata.org/wiki/Special:EntityData/{term}.json"
        entResp = req.urlopen(entRequest.format(term=sqid)).read()
        jpage = json.loads(entResp.decode('UTF-8'))
        return jpage['entities']

    def getHierarchy(self):
        """
        Collects the Hierarchy
        :return:
        """
        qry ="""select ?superclass ?superclassLabel where {{ wd:{term} (wdt:P31/wdt:P279*) ?superclass
                                          SERVICE wikibase:label {{bd:serviceParam wikibase:language "en" .
        }}}}""".format(term=self.sqid)
        self.sparql.setQuery(qry)
        result = self.sparql.query().convert()
        result = result['results']['bindings']

        hier = [d['superclassLabel']['value'] for d in result if 'superclassLabel' in d.keys()]
        return hier

    def propertyLookup(self):
        qry = """SELECT ?property ?propLabel ?object ?objectLabel
            WHERE {{
              hint:Query hint:optimizer "None".
              wd:{sqid} ?property ?object.
               ?prop wikibase:directClaim ?property
               SERVICE wikibase:label {{
                bd:serviceParam wikibase:language "en" .}}
        }}""".format(sqid=self.sqid)
        self.sparql.setQuery(qry)
        attributes = self.sparql.query().convert()
        result = attributes['results']['bindings']
        props = [d['propLabel']['value'] for d in result if 'propLabel' in d.keys()]
        properties = dict()
        for prop in props:
            if prop not in properties.keys():
                properties[prop] = 1
            else:
                properties[prop] += 1

        sorted_list = sorted(properties.items(), key=lambda x: x[1], reverse=True)
        return sorted_list

    def getAge(self):
        print("Sqid is : " +self.sqid)
        qry = """SELECT ?birth_date ?death_date ?age WHERE {{
                VALUES (?sub) {{
                (wd:{})
              }}
              OPTIONAL {{ ?sub wdt:P569 ?birth_date.}}
              OPTIONAL {{ ?sub wdt:P570 ?death_date. }}
              OPTIONAL {{ ?sub wdt:P571 ?birth_date. }}
                BIND( year(?death_date) - year(?birth_date) - if(month(?death_date)<month(?birth_date) || (month(?death_date)=month(?birth_date) && day(?death_date)<day(?birth_date)),1,0) as ?age  )
                Bind(year(?age) as ?year)
            }}""".format(self.sqid)
        self.sparql.setQuery(qry)
        dtInformation = self.sparql.query().convert()
        dtInformation=dtInformation['results']['bindings'][0]
        self.dod=dtInformation['death_date']['value']
        self.age=dtInformation['age']['value']
        self.dob=dtInformation['birth_date']['value']
        print("Age: " +self.age) # Add actual time/date


wl = WikidataLink(TranslateSQID("Bud Spencer").sqid)

# wl.getEntityData()
print("Hierarchy: "+str(wl.getHierarchy()))
properties = wl.propertyLookup()
print("Property lookup: " + str(properties))

print("Getting age")
wl.getAge()

"""
    #Properties, their labels, descriptions and if they exist of which they are a subproperty
#added before 2016-10

SELECT Distinct ?property ?propertyLabel ?subpropertyOfLabel
WHERE
{
    ?property a wikibase:Property .
    OPTIONAL {?property wdt:P1647  ?subpropertyOf .}
    SERVICE wikibase:label { bd:serviceParam wikibase:language "en" }
}"""

"""SELECT ?subLabel ?birth_date ?death_date ?incp ?age ?year WHERE {
  VALUES (?sub) {
    (wd:Q64)
  }
  OPTIONAL { ?sub wdt:P569 ?birth_date.}
  OPTIONAL { ?sub wdt:P570 ?death_date. }
  OPTIONAL { ?sub wdt:P571 ?birth_date. }
  SERVICE wikibase:label { bd:serviceParam wikibase:language "en,en". }
BIND( year(?death_date) - year(?birth_date) - if(month(?death_date)<month(?birth_date) || (month(?death_date)=month(?birth_date) && day(?death_date)<day(?birth_date)),1,0) as ?age  )
Bind(year(?age) as ?year)
}"""