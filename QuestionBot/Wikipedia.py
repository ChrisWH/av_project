import wikipedia

def tag_term(term):
    wiki = (str(wikipedia.summary(term)))
    wiki = wiki.lower()
    for i in ['\n', '.', ',', '?', '!', ';', '-', '/', '"', '(', ')']:
        wiki = wiki.replace(i, ' ')
    wiki_arry = wiki.split(' ')

    with open('en_stopwords.txt') as stop:
        stopwords = set(stop.read().split('\n'))

        wiki_arry = [w for w in wiki_arry if w not in stopwords]
        wiki_arry = [w for w in wiki_arry if not (w.startswith('[') or w.endswith(']'))]
        for i in range(len(wiki_arry)):
            if wiki_arry[i].startswith("'"):
                wiki_arry[i] = wiki_arry[i][1:]
            if wiki_arry[i].endswith("'"):
                wiki_arry[i] = wiki_arry[i][:-1]

        wiki_arry = [w for w in wiki_arry if w if len(w) > 3]
    wiki_dict = dict()
    for word in wiki_arry:
        if word not in wiki_dict.keys():
            wiki_dict[word] = 1
        else:
            wiki_dict[word] += 1

    sorted_list=sorted(wiki_dict.items(), key=lambda x: x[1], reverse=True)
    return sorted_list


def search_verbs(file_name):
    s = set()

    with open(file_name, 'r') as file:
        for line in file.read().split('\n'):
            for rawWord in line.split(' '):
                for word in rawWord .split('/'):
                    s.add(word.lower())
    if '' in s:
        s.remove('')
    return s


class Verbs:
    def __init__(self):
        self.RootForms = set(search_verbs('en_600regularVerbs.txt'))
        with open('en_CommonIregularVerbs.txt', 'r') as irrVerbs:
            verbs = irrVerbs.read().split('\n')[1:]
            self.irrRootForms = [v.strip().lower().replace('/', ' ').split(' ') for v in verbs]
        print(self.irrRootForms)

    def conjugate(self, verb):  # check for verb in irregular verb list
        verbForms = list()
        verbForms.append(verb)
        for irVerbs in self.irrRootForms:
            if verb in irVerbs:
                return irVerbs

        if verb[-1] in 'mpd' and verb[-2] in 'aeiou':
            verbForms.append(verb + verb[-1] + 'ing')
        else:
            verbForms.append(verb + 'ing' if not verb.endswith('e') else verb[:-1] + 'ing')

        if verb.endswith('y'):
            verb = verb[:-1] + 'ie'
        verbForms.append(verb + 's' if not (verb.endswith('s') or verb.endswith('sh')) else verb + 'es')

        if verb[-1] in 'mpd' and verb[-2] in 'aeiou':
            verbForms.append(verb + verb[-1] + 'ed')
        else:
            verbForms.append(verb + 'ed' if not verb.endswith('e') else verb + 'd')
        return verbForms

    def getVerbRoot(self, verb):
        if verb in self.RootForms:
            return verb
        for rVerb in self.RootForms:
            if verb in self.conjugate(rVerb):
                return rVerb
        for irVerbs in self.irrRootForms:
            if verb in irVerbs:
                return irVerbs[0]
        return None

class words:
    def __init__(self, term):
        self.wordlist = tag_term(term)
        self.verbs = set()
    def splitText(self):
        v = Verbs()
        for word in self.wordlist:
            root = v.getVerbRoot(word[0])
            print(word)
            if root:
                self.verbs.add(root)
                print(root + " is a verb")

def combinedTerms(termList):
    retList = list(termList)
    for i in range(len(termList)-1):
        retList.append((termList[i] + ' ' + termList[i+1]))
    for i in range(len(termList)-2):
        retList.append((termList[i] + ' ' + termList[i+1] + ' ' + termList[i+2]))
    return retList


def categorizeTerms(terms):
    categories = dict()
    prvTerm = ""
    for term in combinedTerms(terms):
        try:
            def lookupTerm(term):
                print(term)
                page = wikipedia.page(term)
                print(page)
                for word in page.categories:
                    if word not in categories.keys():
                        categories[word] = 1
                    else:
                        categories[word] += 1
            lookupTerm(term)
        except wikipedia.DisambiguationError as des:
            """try:
                print(des.options)
                lookupTerm(prvTerm + ' '+ term)
            except wikipedia.WikipediaException:"""
            pass
        except wikipedia.exceptions.WikipediaException:
            pass
        # prvTerm = term
    return categories

def suggestWikiPage(term):
    qry = wikipedia.search(term)
    print("The search returned "+ str(qry))
    wp = wikipedia.page(term)
    print(wp.title)

    return wp

# Did Clint Eastwood ever drive a fiat to the mountains with john wayne and Angelina Jolie
terms = "Oak Birch Acorn ironwood redwood".split(' ')
cat = categorizeTerms(terms)
cat=sorted(cat.items(), key=lambda x: x[1], reverse=True)
print(cat)
'''
verb = 'swim', 'swam'
v = Verbs()
root = v.getVerbRoot(verb[1])
print(root)
print(v.conjugate(verb[0]))

common = search_verbs('en_CommonRegularVerbs.txt')
for c in [conjugate(common)]:
    print(c)
print(search_verbs('en_CommonIregularVerbs.txt'))'''

#print(tag_term("Twain"))

#splitText("New York")