﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.ListViewItem;

namespace GUI_WinForms
{
    public partial class Form1 : Form
    {
        Logic dataset = new Logic();
        public Form1()
        {
            InitializeComponent();
            comboBox1.Items.AddRange(dataset.AllTeams.ToArray());

        }

        private void button1_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawEllipse(new Pen(Color.AliceBlue), 0,0,20,20 );
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawEllipse(new Pen(Color.ForestGreen), 0, 0, 20, 20);
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            listView1.Items.Clear();
            if (comboBox1.SelectedIndex >= 0)
            {
                var table = dataset.getFilteredTableByTeam(comboBox1.SelectedItem.ToString());
                foreach (var match in table)
                {
                    var item = listView1.Items.Add(match.Team1);
                    item.SubItems.Add(match.Team2);
                    item.SubItems.Add(match.Result);
                }

            }
        }
    }
}
