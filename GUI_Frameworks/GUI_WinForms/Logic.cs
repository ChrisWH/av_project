﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace GUI_WinForms
{
    class Logic : INotifyPropertyChanged
    {
        public List<Match> table = new List<Match> { new Match{ Team1 = "Island", Team2 = "England", Result = "2:1" },
        new Match{ Team1 = "Italien", Team2 = "Spanien", Result = "2:0" } ,
        new Match{ Team1 = "England", Team2 = "Italien", Result = "0:5" }};

        public HashSet<string> AllTeams
        {
            get
            {
                HashSet<string> Result = new HashSet<string>();
                foreach (var item in table)
                {
                    Result.Add(item.Team1);
                    Result.Add(item.Team2);
                }
                return Result;
            }
            
        }
        private String _currentTeam; 
        public String CurrentTeam
        {
            get
            {
                return _currentTeam;
            }
            set
            {
                _currentTeam = value;
                NotifyPropertyChanged("FilteredTableByTeam");
            }
        }
        public IEnumerable<Match> FilteredTableByTeam { get { return getFilteredTableByTeam(CurrentTeam); } }

        public event PropertyChangedEventHandler PropertyChanged;

        // This method is called by the Set accessor of each property.
        // The CallerMemberName attribute that is applied to the optional propertyName
        // parameter causes the property name of the caller to be substituted as an argument.
        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }


        public IEnumerable<Match> getFilteredTableByTeam(string Team)
        {
            return table.Where(item => item.Team1 == Team || item.Team2 == Team);
        }
    }
}
