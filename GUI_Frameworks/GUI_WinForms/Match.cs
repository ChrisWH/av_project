﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GUI_WinForms
{
    class Match
    {
        public string Team1 { get; set; }
        public string Team2 { get; set; }
        public string Result { get; set; }
        public DateTime Date { get; set; }

        public override string ToString()
        {
            return Team1 +"-" + Team2 + "   "+ Result;
        }
    }
}
